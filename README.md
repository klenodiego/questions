# Juego de preguntas
## pruaba tecnica para sofka. Por Diego Patino

# Para ejecutar el Juego
- Abrir la solucion en visual studio
- Abrir el archivo `appsettings.json` y cambiar la cadena de conexion al servidor Sql Server destino.
- Abrir el archivo `Repositories/QuestionsContext.cs` y reemplazar las preguntas y respuestas por las deseadas. No se deben modificar los Ids ni las claves foraneas.
- En la linea de comandos ir a la carpeta root del proyecto y ejecutar el comando `dotnet ef database update`
- Ejecutar el proyecto en visual studio.
- Ir al folder `Frontend` y Abrir el archivo `Questions.html`.
- Disfruta el juego!.

function Questions(mainContentId, nextButtonId, correctCounterId, incorrectCounterId){
    this.questions = [];
    this.isCurrentAnswerOk = false;
    this.currentQuestion = 0;
    this.correctAnswers = 0;
    this.incorrectAnswers = 0;
    this.mainContent = document.getElementById(mainContentId);
    this.nextButton = document.getElementById(nextButtonId);
    this.correctCounter = document.getElementById(correctCounterId);
    this.incorrectCounter = document.getElementById(incorrectCounterId);

    $.ajax({
        url: "https://localhost:7275/questions",
      }).done((response) => {
        this.questions = response;
        this.createQuestionForm();
      });
    
    this.nextButton.onclick = () => {
        displayNextQuestion();
    }

    this.createQuestionForm = () => {
        this.mainContent.innerHTML = "";
        this.mainContent.appendChild(createQuestion(this.questions[this.currentQuestion].label));
        this.mainContent.appendChild(createAnswers(this.questions[this.currentQuestion].answers));
        setScore();
    }

    createQuestion = (text) => {
        const questionDiv = document.createElement("div");
        questionDiv.className = "row";
        const questionSpan = document.createElement("span");
        questionSpan.innerHTML = text;
        questionDiv.appendChild(questionSpan);
        return questionDiv;
    }

    createAnswers = (answers) => {
        const answersDiv = document.createElement("div");
        answersDiv.className = "row";
        for(let i = 0; i < answers.length; i++) {
            answersDiv.appendChild(createAnswer(answers[i]));
        }
        return answersDiv;
    }

    createAnswer = (answer) => {
        const answerRoot = document.createElement("div");        
        answerRoot.className = "row";
        const answerDiv = document.createElement("div");
        answerDiv.className = "col col-md-12";
        const answerLabel = document.createElement("label");
        answerLabel.innerHTML = answer.label;
        answerLabel.htmlFor = answer.id;
        const answerRadio = document.createElement("input");
        answerRadio.type = "radio"
        answerRadio.name = "answer"
        answerRadio.id = answer.id;
        answerRadio.isCorrect = answer.isCorrect;
        answerRadio.onclick = () => {
            this.isCurrentAnswerOk = event.srcElement.isCorrect;
        }
        answerRoot.appendChild(answerDiv);
        answerDiv.appendChild(answerRadio);
        answerDiv.appendChild(answerLabel);
        return answerRoot;
    }

    displayNextQuestion = () => {
        if (this.isCurrentAnswerOk)
            this.correctAnswers++;
        else
            this.incorrectAnswers++;

        this.isCurrentAnswerOk = false;
        this.currentQuestion++;
        if (this.questions[this.currentQuestion])
            this.createQuestionForm();
        else
            window.location = "./GameOver.html?correct=" + this.correctAnswers.toString() + "&incorrect=" + this.incorrectAnswers.toString();
    }

    setScore = () => {
        this.correctCounter.innerHTML = this.correctAnswers;
        this.incorrectCounter.innerHTML = this.incorrectAnswers;
    }
}
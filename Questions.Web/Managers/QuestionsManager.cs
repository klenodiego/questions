using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Questions.Web.Extensions;
using Questions.Web.Models;
using Questions.Web.Repositories;

namespace Questions.Web.Managers
{
    public class QuestionsManager
    {
        private readonly QuestionsContext Db;
        public QuestionsManager(QuestionsContext db)
        {
            Db = db ?? throw new ArgumentNullException(nameof(db));
        }
        
        public IEnumerable<Question> GetAllQuestions()
        {
            var questions = Db.Questions.Include(i => i.Answers).ToList();
            questions = questions.Shuffle().ToList();
            foreach (var question in questions)
            {
                question.Answers = question.Answers.Shuffle();
            }
            return questions;
        }
    }
}
namespace Questions.Web.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Label { get; set; }
        public bool IsCorrect { get; set; }

        public Question Question { get; set; }
    }
}
using System.Collections.Generic;

namespace Questions.Web.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public IEnumerable<Answer> Answers { get; set; }
    }
}
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Questions.Web.Managers;
using Questions.Web.Models;

namespace Questions.Web.Controllers
{
    public class QuestionsController : Controller
    {
        private readonly QuestionsManager QuestionsManager;
        public QuestionsController(QuestionsManager questionsManager)
        {
            QuestionsManager = questionsManager ?? throw new ArgumentNullException(nameof(questionsManager));
        }
        
        [HttpGet("questions")]
        public IEnumerable<Question> Get()
        {
            return QuestionsManager.GetAllQuestions();
        }
    }
}
﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Questions.Web.Migrations
{
    public partial class AddData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "Label" },
                values: new object[,]
                {
                    { 1, "Pregunta 1" },
                    { 2, "Pregunta 2" },
                    { 3, "Pregunta 3" },
                    { 4, "Pregunta 4" },
                    { 5, "Pregunta 5" },
                    { 6, "Pregunta 6" },
                    { 7, "Pregunta 7" },
                    { 8, "Pregunta 8" },
                    { 9, "Pregunta 9" },
                    { 10, "Pregunta 10" },
                    { 11, "Pregunta 11" },
                    { 12, "Pregunta 12" },
                    { 13, "Pregunta 13" },
                    { 14, "Pregunta 14" },
                    { 15, "Pregunta 15" },
                    { 16, "Pregunta 16" },
                    { 17, "Pregunta 17" },
                    { 18, "Pregunta 18" },
                    { 19, "Pregunta 19" },
                    { 20, "Pregunta 20" },
                    { 21, "Pregunta 21" },
                    { 22, "Pregunta 22" },
                    { 23, "Pregunta 23" },
                    { 24, "Pregunta 24" },
                    { 25, "Pregunta 25" }
                });

            migrationBuilder.InsertData(
                table: "Answers",
                columns: new[] { "Id", "IsCorrect", "Label", "QuestionId" },
                values: new object[,]
                {
                    { 1, true, "Respuesta correcta", 1 },
                    { 2, false, "Respuesta incorrecta", 1 },
                    { 3, false, "Respuesta incorrecta", 1 },
                    { 4, false, "Respuesta incorrecta", 1 },
                    { 5, true, "Respuesta correcta", 2 },
                    { 6, false, "Respuesta incorrecta", 2 },
                    { 7, false, "Respuesta incorrecta", 2 },
                    { 8, false, "Respuesta incorrecta", 2 },
                    { 9, true, "Respuesta correcta", 3 },
                    { 10, false, "Respuesta incorrecta", 3 },
                    { 11, false, "Respuesta incorrecta", 3 },
                    { 12, false, "Respuesta incorrecta", 3 },
                    { 13, true, "Respuesta correcta", 4 },
                    { 14, false, "Respuesta incorrecta", 4 },
                    { 15, false, "Respuesta incorrecta", 4 },
                    { 16, false, "Respuesta incorrecta", 4 },
                    { 17, true, "Respuesta correcta", 5 },
                    { 18, false, "Respuesta incorrecta", 5 },
                    { 19, false, "Respuesta incorrecta", 5 },
                    { 20, false, "Respuesta incorrecta", 5 },
                    { 21, true, "Respuesta correcta", 6 },
                    { 22, false, "Respuesta incorrecta", 6 },
                    { 23, false, "Respuesta incorrecta", 6 },
                    { 24, false, "Respuesta incorrecta", 6 },
                    { 25, true, "Respuesta correcta", 7 },
                    { 26, false, "Respuesta incorrecta", 7 },
                    { 27, false, "Respuesta incorrecta", 7 },
                    { 28, false, "Respuesta incorrecta", 7 },
                    { 29, true, "Respuesta correcta", 8 },
                    { 30, false, "Respuesta incorrecta", 8 },
                    { 31, false, "Respuesta incorrecta", 8 },
                    { 32, false, "Respuesta incorrecta", 8 },
                    { 33, true, "Respuesta correcta", 9 },
                    { 34, false, "Respuesta incorrecta", 9 },
                    { 35, false, "Respuesta incorrecta", 9 },
                    { 36, false, "Respuesta incorrecta", 9 },
                    { 37, true, "Respuesta correcta", 10 },
                    { 38, false, "Respuesta incorrecta", 10 },
                    { 39, false, "Respuesta incorrecta", 10 },
                    { 40, false, "Respuesta incorrecta", 10 },
                    { 41, true, "Respuesta correcta", 11 },
                    { 42, false, "Respuesta incorrecta", 11 }
                });

            migrationBuilder.InsertData(
                table: "Answers",
                columns: new[] { "Id", "IsCorrect", "Label", "QuestionId" },
                values: new object[,]
                {
                    { 43, false, "Respuesta incorrecta", 11 },
                    { 44, false, "Respuesta incorrecta", 11 },
                    { 45, true, "Respuesta correcta", 12 },
                    { 46, false, "Respuesta incorrecta", 12 },
                    { 47, false, "Respuesta incorrecta", 12 },
                    { 48, false, "Respuesta incorrecta", 12 },
                    { 49, true, "Respuesta correcta", 13 },
                    { 50, false, "Respuesta incorrecta", 13 },
                    { 51, false, "Respuesta incorrecta", 13 },
                    { 52, false, "Respuesta incorrecta", 13 },
                    { 53, true, "Respuesta correcta", 14 },
                    { 54, false, "Respuesta incorrecta", 14 },
                    { 55, false, "Respuesta incorrecta", 14 },
                    { 56, false, "Respuesta incorrecta", 14 },
                    { 57, true, "Respuesta correcta", 15 },
                    { 58, false, "Respuesta incorrecta", 15 },
                    { 59, false, "Respuesta incorrecta", 15 },
                    { 60, false, "Respuesta incorrecta", 15 },
                    { 61, true, "Respuesta correcta", 16 },
                    { 62, false, "Respuesta incorrecta", 16 },
                    { 63, false, "Respuesta incorrecta", 16 },
                    { 64, false, "Respuesta incorrecta", 16 },
                    { 65, true, "Respuesta correcta", 17 },
                    { 66, false, "Respuesta incorrecta", 17 },
                    { 67, false, "Respuesta incorrecta", 17 },
                    { 68, false, "Respuesta incorrecta", 17 },
                    { 69, true, "Respuesta correcta", 18 },
                    { 70, false, "Respuesta incorrecta", 18 },
                    { 71, false, "Respuesta incorrecta", 18 },
                    { 72, false, "Respuesta incorrecta", 18 },
                    { 73, true, "Respuesta correcta", 19 },
                    { 74, false, "Respuesta incorrecta", 19 },
                    { 75, false, "Respuesta incorrecta", 19 },
                    { 76, false, "Respuesta incorrecta", 19 },
                    { 77, true, "Respuesta correcta", 20 },
                    { 78, false, "Respuesta incorrecta", 20 },
                    { 79, false, "Respuesta incorrecta", 20 },
                    { 80, false, "Respuesta incorrecta", 20 },
                    { 81, true, "Respuesta correcta", 21 },
                    { 82, false, "Respuesta incorrecta", 21 },
                    { 83, false, "Respuesta incorrecta", 21 },
                    { 84, false, "Respuesta incorrecta", 21 }
                });

            migrationBuilder.InsertData(
                table: "Answers",
                columns: new[] { "Id", "IsCorrect", "Label", "QuestionId" },
                values: new object[,]
                {
                    { 85, true, "Respuesta correcta", 22 },
                    { 86, false, "Respuesta incorrecta", 22 },
                    { 87, false, "Respuesta incorrecta", 22 },
                    { 88, false, "Respuesta incorrecta", 22 },
                    { 89, true, "Respuesta correcta", 23 },
                    { 90, false, "Respuesta incorrecta", 23 },
                    { 91, false, "Respuesta incorrecta", 23 },
                    { 92, false, "Respuesta incorrecta", 23 },
                    { 93, true, "Respuesta correcta", 24 },
                    { 94, false, "Respuesta incorrecta", 24 },
                    { 95, false, "Respuesta incorrecta", 24 },
                    { 96, false, "Respuesta incorrecta", 24 },
                    { 97, true, "Respuesta correcta", 25 },
                    { 98, false, "Respuesta incorrecta", 25 },
                    { 99, false, "Respuesta incorrecta", 25 },
                    { 100, false, "Respuesta incorrecta", 25 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Answers",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 25);
        }
    }
}
